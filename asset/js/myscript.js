// slider
window.onload=function(){
  $('.c-slider').slick({
  autoplay:true,
  autoplaySpeed:1500,
  arrows:true,
  prevArrow:'<button type="button" class="slick-prev"><img src="../img/top_slide_prev.png"></button>',
  nextArrow:'<button type="button" class="slick-next"><img src="../img/top_slide_next.png"></button>',
  slidesToShow:1,
  slidesToScroll:1
  });
};